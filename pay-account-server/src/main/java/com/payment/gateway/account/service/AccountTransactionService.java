package com.payment.gateway.account.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.payment.gateway.storage.po.AccountEntry;
import com.payment.gateway.storage.po.AccountTransaction;

import java.util.List;

public interface AccountTransactionService extends IService<AccountTransaction> {

    void initTransaction(AccountTransaction accountTransaction, List<AccountEntry> accountEntries);

    void updateActTransaction(AccountTransaction accountTransaction);
}
