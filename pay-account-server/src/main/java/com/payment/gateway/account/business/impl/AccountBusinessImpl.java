package com.payment.gateway.account.business.impl;

import com.payment.gateway.account.service.AccountRedisService;
import com.payment.gateway.account.service.AccountService;
import com.payment.gateway.account.service.AccountTransactionService;
import com.payment.gateway.account.business.AccountBusiness;
import com.payment.gateway.common.base.*;
import com.payment.gateway.common.constants.AccountBusinessCode;
import com.payment.gateway.common.constants.MessageConstant;
import com.payment.gateway.common.dto.account.AccountOperateRequest;
import com.payment.gateway.common.dto.account.AccountRealTimeResult;
import com.payment.gateway.common.dto.account.AccountRecordRequest;
import com.payment.gateway.common.dto.account.AccountRecordResult;
import com.payment.gateway.common.exception.BusinessException;
import com.payment.gateway.common.utils.GeneralUtils;
import com.payment.gateway.storage.po.Account;
import com.payment.gateway.storage.po.AccountEntry;
import com.payment.gateway.storage.po.AccountTransaction;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@Service
public class AccountBusinessImpl implements AccountBusiness {
    private final Logger logger = LoggerFactory.getLogger(this.getClass());

    @Autowired
    private AccountRedisService accountRedisService;

    @Autowired
    private AccountService accountService;

    @Autowired
    private AccountTransactionService accountTransactionService;

    @Autowired
    private RabbitTemplate rabbitTemplate;

    @Override
    public AccountRealTimeResult operateAccount(AccountOperateRequest accountOperateRequest) {
        AccountRealTimeResult accountRealTimeResult = null;
        if (AccountOperate.ACT_OPENING.equals(accountOperateRequest.getAccountOperate())) {
            try {
                accountService.openingAccount(accountOperateRequest.getMemberNo())
                        .forEach(account -> accountRedisService.setActIdForMemberNo(account));
            } catch (Exception e) {
                logger.error("执行账户开通数据操作异常", e);
                throw new BusinessException(AccountBusinessCode.ACCOUNT_DATABASE_EXCEPTION);
            }
            logger.info("执行账户开通数据操作完成：{}", accountOperateRequest.getMemberNo());

        } else if (AccountOperate.ACT_QUERY.equals(accountOperateRequest.getAccountOperate())) {
            Account account = null;
            try {
                account = this.getAccount(accountOperateRequest.getMemberNo(), accountOperateRequest.getAccountType());
            } catch (Exception e) {
                logger.error("查询账户信息异常", e);
                throw new BusinessException(AccountBusinessCode.ACCOUNT_DATABASE_EXCEPTION);
            }
            logger.info("执行账户查询数据操作完成：{}", account.toString());

            accountRealTimeResult = GeneralUtils.convertTarget(account, AccountRealTimeResult::new);
        } else {
            try {
                this.frozenAccount(accountOperateRequest.getMemberNo(), accountOperateRequest.getAccountType());
            } catch (Exception e) {
                logger.error("执行账户冻结数据操作异常", e);
                throw new BusinessException(AccountBusinessCode.ACCOUNT_REDIS_OPERATE_EXCEPTION);
            }
            logger.info("执行账户冻结数据操作完成：{}.{}", accountOperateRequest.getMemberNo(), accountOperateRequest.getAccountType());

        }

        return accountRealTimeResult;
    }

    private Account getAccount(String memberNo, AccountType accountType) {
        String actId = accountRedisService.getActIdByMemberNo(memberNo, accountType);
        if (StringUtils.isEmpty(actId)) {
            throw new BusinessException(AccountBusinessCode.ACCOUNT_REDIS_OPERATE_EXCEPTION);
        }
        return accountService.getById(Long.valueOf(actId));
    }

    private void frozenAccount(String memberNo, AccountType accountType) {
        String actId = accountRedisService.getActIdByMemberNo(memberNo, accountType);
        if (StringUtils.isEmpty(actId)) {
            throw new BusinessException(AccountBusinessCode.ACCOUNT_REDIS_OPERATE_EXCEPTION);
        }
        Account account = new Account();
        account.setActId(Long.valueOf(actId));
        account.setStatus(AccountStatus.ACT_FREEZE);
        accountService.updateAccount(account);
    }

    @Override
    public AccountRecordResult executeRecord(AccountRecordRequest accountRecordRequest) {
        AccountTransaction accountTransaction = GeneralUtils.convertTarget(accountRecordRequest, AccountTransaction::new);
        logger.info("构造入账事务实体信息为：{}", accountTransaction.toString());

        List<AccountEntry> accountEntries = null;
        try {
            accountEntries = this.structureActEntry(accountRecordRequest);
        } catch (Exception e) {
            logger.error("构造入账分录实体列表异常", e);
            throw new BusinessException(AccountBusinessCode.STRUCTURE_ENTRY_EXCEPTION);
        }
        logger.info("构造入账分录实体列表为：{}", accountEntries.toString());

        try {
            accountTransactionService.initTransaction(accountTransaction, accountEntries);
        } catch (Exception e) {
            logger.error("初始化入账事务及分录异常", e);
            throw new BusinessException(AccountBusinessCode.INIT_ACT_TRANS_EXCEPTION);
        }
        AccountRecordResult accountRecordResult = GeneralUtils.convertTarget(accountTransaction, AccountRecordResult::new);

        try {
            rabbitTemplate.convertAndSend(MessageConstant.MESSAGE_EXCHANGE, MessageConstant.MESSAGE_ROUTING_KEY, accountTransaction);
        } catch (Exception e) {
            throw new BusinessException(AccountBusinessCode.TRANS_MESSAGE_QUEUE_EXCEPTION);
        }
        logger.info("入账事务发送到消息队列：{}.{}完成", MessageConstant.MESSAGE_EXCHANGE, MessageConstant.MESSAGE_ROUTING_KEY);
        return accountRecordResult;
    }

    private List<AccountEntry> structureActEntry(AccountRecordRequest accountRecordRequest) {
        if (StringUtils.isEmpty(accountRecordRequest.getContextTransId())) {
            switch (accountRecordRequest.getSubjectType()) {
                case BANK_CARD_COLLECTION:
                    return Stream.of(structureActEntry(accountRecordRequest, AccountType.BANK_ACCOUNT, BalanceDirection.RECHARGE),
                            structureActEntry(accountRecordRequest, AccountType.INTERMEDIATE_CASHIER, BalanceDirection.RECHARGE))
                            .collect(Collectors.toList());
                case BALANCE_COLLECTION:
                    return Stream.of(structureActEntry(accountRecordRequest, AccountType.CUSTOMER_BASE_ACT, BalanceDirection.DEDUCTION),
                            structureActEntry(accountRecordRequest, AccountType.INTERMEDIATE_CASHIER, BalanceDirection.RECHARGE))
                            .collect(Collectors.toList());
                case PAYMENT_BANK_CARD:
                case PAYMENT_BALANCE:
                    return Stream.of(structureActEntry(accountRecordRequest, AccountType.INSTITUTIONAL_ACT, BalanceDirection.DEDUCTION),
                            structureActEntry(accountRecordRequest, AccountType.INTERMEDIATE_PAYER, BalanceDirection.RECHARGE))
                            .collect(Collectors.toList());
                default:
                    throw new BusinessException(AccountBusinessCode.STRUCTURE_ENTRY_EXCEPTION);
            }
        } else {
            switch (accountRecordRequest.getSubjectType()) {
                case BANK_CARD_COLLECTION:
                case BALANCE_COLLECTION:
                    return Stream.of(structureActEntry(accountRecordRequest, AccountType.INTERMEDIATE_CASHIER, BalanceDirection.DEDUCTION),
                            structureActEntry(accountRecordRequest, AccountType.INSTITUTIONAL_ACT, BalanceDirection.RECHARGE))
                            .collect(Collectors.toList());
                case PAYMENT_BANK_CARD:
                    return Stream.of(structureActEntry(accountRecordRequest, AccountType.INTERMEDIATE_PAYER, BalanceDirection.DEDUCTION),
                            structureActEntry(accountRecordRequest, AccountType.BANK_ACCOUNT, BalanceDirection.DEDUCTION))
                            .collect(Collectors.toList());
                case PAYMENT_BALANCE:
                    return Stream.of(structureActEntry(accountRecordRequest, AccountType.INTERMEDIATE_PAYER, BalanceDirection.DEDUCTION),
                            structureActEntry(accountRecordRequest, AccountType.BUSINESS_PAY_ACT, BalanceDirection.RECHARGE))
                            .collect(Collectors.toList());
                default:
                    throw new BusinessException(AccountBusinessCode.STRUCTURE_ENTRY_EXCEPTION);
            }
        }
    }

    private AccountEntry structureActEntry(AccountRecordRequest accountRecordRequest, AccountType accountType, BalanceDirection balanceDirection) {
        AccountEntry accountEntry = GeneralUtils.convertTarget(accountRecordRequest, AccountEntry::new);
        accountEntry.setEntryAmount(accountRecordRequest.getTradeAmount());

        String actId = accountRedisService.getActIdByMemberNo(accountRecordRequest.getMemberNo(), accountType);
        if (StringUtils.isEmpty(actId)) {
            throw new BusinessException(AccountBusinessCode.ACCOUNT_NULL_EXCEPTION);
        }
        accountEntry.setActId(Long.valueOf(actId));
        accountEntry.setBalanceDirection(balanceDirection);
        return accountEntry;
    }

}
