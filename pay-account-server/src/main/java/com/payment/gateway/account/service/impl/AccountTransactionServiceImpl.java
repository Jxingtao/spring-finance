package com.payment.gateway.account.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.payment.gateway.account.service.AccountTransactionService;
import com.payment.gateway.common.base.AccountStatus;
import com.payment.gateway.common.constants.AccountBusinessCode;
import com.payment.gateway.common.utils.GeneralUtils;
import com.payment.gateway.storage.mapper.AccountEntryMapper;
import com.payment.gateway.storage.mapper.AccountTransactionMapper;
import com.payment.gateway.storage.po.AccountEntry;
import com.payment.gateway.storage.po.AccountTransaction;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import java.util.Date;
import java.util.List;

@Service
public class AccountTransactionServiceImpl extends ServiceImpl<AccountTransactionMapper, AccountTransaction> implements AccountTransactionService {

    @Autowired
    private AccountEntryMapper accountEntryMapper;

    @Override
    @Transactional(propagation = Propagation.REQUIRED)
    public void initTransaction(AccountTransaction accountTransaction, List<AccountEntry> accountEntries) {
        accountTransaction.setStatus(AccountStatus.INITIALIZE);
        this.saveEntity(accountTransaction);

        accountEntries.forEach(accountEntry -> {
            accountEntry.setEntryId(GeneralUtils.generateUniqueId());
            accountEntry.setStatus(AccountStatus.INITIALIZE);
            accountEntry.setCreateTime(new Date());
            accountEntry.setUpdateTime(new Date());
            int count = accountEntryMapper.insert(accountEntry);
            if (count < 1) {
                throw new RuntimeException(AccountBusinessCode.INIT_ACT_TRANS_EXCEPTION.getMessage());
            }
        });
    }

    private void saveEntity(AccountTransaction accountTransaction) {
        accountTransaction.setCreateTime(new Date());
        accountTransaction.setUpdateTime(new Date());
        boolean result = super.save(accountTransaction);
        if (!result) {
            throw new RuntimeException(AccountBusinessCode.INIT_ACT_TRANS_EXCEPTION.getMessage());
        }
    }

    @Override
    public void updateActTransaction(AccountTransaction accountTransaction) {
        accountTransaction.setUpdateTime(new Date());
        boolean result = super.updateById(accountTransaction);
        if (!result) {
            throw new RuntimeException(AccountBusinessCode.ACCOUNT_DATABASE_EXCEPTION.getMessage());
        }

    }
}
