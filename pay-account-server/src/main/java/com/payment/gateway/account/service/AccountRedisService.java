package com.payment.gateway.account.service;

import com.payment.gateway.common.base.AccountType;
import com.payment.gateway.storage.po.Account;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;

@Component
public class AccountRedisService {

    @Autowired
    private StringRedisTemplate stringRedisTemplate;

    @Autowired
    private AccountService accountService;

    @PostConstruct
    public void initAccountActId() {
        accountService.list().forEach(this::setActIdForMemberNo);
    }

    public void setActIdForMemberNo(Account account) {
        stringRedisTemplate.opsForHash().put(account.getMemberNo(), account.getAccountType().getCode(), String.valueOf(account.getActId()));
    }

    public String getActIdByMemberNo(String memberNo, AccountType accountType) {
        return (String) stringRedisTemplate.opsForHash().get(memberNo, accountType.getCode());
    }
}
