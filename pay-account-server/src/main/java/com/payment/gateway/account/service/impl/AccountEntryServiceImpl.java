package com.payment.gateway.account.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.payment.gateway.account.service.AccountEntryService;
import com.payment.gateway.common.base.AccountStatus;
import com.payment.gateway.common.base.BalanceDirection;
import com.payment.gateway.common.constants.AccountBusinessCode;
import com.payment.gateway.storage.mapper.AccountEntryMapper;
import com.payment.gateway.storage.mapper.AccountMapper;
import com.payment.gateway.storage.mapper.AccountTransactionMapper;
import com.payment.gateway.storage.po.Account;
import com.payment.gateway.storage.po.AccountEntry;
import com.payment.gateway.storage.po.AccountTransaction;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.ObjectUtils;

import java.util.Date;
import java.util.List;

@Service
public class AccountEntryServiceImpl extends ServiceImpl<AccountEntryMapper, AccountEntry> implements AccountEntryService {

    @Autowired
    private AccountMapper accountMapper;

    @Autowired
    private AccountTransactionMapper accountTransactionMapper;

    @Override
    public List<AccountEntry> getAccountEntries(Long actTransId) {
        return super.list(new QueryWrapper<AccountEntry>()
                .lambda()
                .eq(AccountEntry::getActTransId, actTransId));
    }

    @Override
    @Transactional(propagation = Propagation.REQUIRED)
    public void processAccountRecord(List<AccountEntry> accountEntries, AccountTransaction accountTransaction) {
        accountEntries.forEach(this::executeAccountEntry);

        accountTransaction.setStatus(AccountStatus.RECORD_SUCCESS);
        accountTransaction.setActTransMsg(AccountStatus.RECORD_SUCCESS.getName());
        accountTransaction.setUpdateTime(new Date());
        int count = accountTransactionMapper.updateById(accountTransaction);
        if (count < 1) {
            throw new RuntimeException(AccountBusinessCode.ACCOUNT_DATABASE_EXCEPTION.getMessage());
        }
    }

    private void executeAccountEntry(AccountEntry accountEntry) {
        Account account = accountMapper.selectById(accountEntry.getActId());
        if (ObjectUtils.isEmpty(account)) {
            throw new RuntimeException(AccountBusinessCode.ACCOUNT_DATABASE_EXCEPTION.getMessage());
        }

        if (BalanceDirection.RECHARGE.equals(accountEntry.getBalanceDirection())) {
            account.setActAmount(account.getActAmount() + accountEntry.getEntryAmount());
        } else {
            account.setActAmount(account.getActAmount() - accountEntry.getEntryAmount());
        }

        account.setStatus(AccountStatus.ACT_NORMAL);
        account.setUpdateTime(new Date());
        int count = accountMapper.updateById(account);
        if (count < 1) {
            throw new RuntimeException(AccountBusinessCode.ACCOUNT_DATABASE_EXCEPTION.getMessage());
        }

        accountEntry.setStatus(AccountStatus.ENTRY_FINISH);
        this.updateEntity(accountEntry);
    }

    private void updateEntity(AccountEntry accountEntry) {
        accountEntry.setUpdateTime(new Date());
        boolean result = super.updateById(accountEntry);
        if (!result) {
            throw new RuntimeException(AccountBusinessCode.ACCOUNT_DATABASE_EXCEPTION.getMessage());
        }
    }
}
