package com.payment.gateway.account.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.payment.gateway.storage.po.Account;

import java.util.List;

public interface AccountService extends IService<Account> {

    List<Account> openingAccount(String memberNo);

    void updateAccount(Account account);
}
