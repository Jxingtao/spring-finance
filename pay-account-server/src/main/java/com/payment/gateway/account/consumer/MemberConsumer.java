package com.payment.gateway.account.consumer;

import com.payment.gateway.account.consumer.fallback.MemberConsumerImpl;
import org.springframework.cloud.openfeign.FeignClient;

@FeignClient(name = "http://member", path = "/member", fallback = MemberConsumerImpl.class)
public interface MemberConsumer {
}
