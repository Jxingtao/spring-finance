package com.payment.gateway.account.api;

import com.payment.gateway.account.business.AccountBusiness;
import com.payment.gateway.common.dto.account.AccountOperateRequest;
import com.payment.gateway.common.dto.account.AccountRealTimeResult;
import com.payment.gateway.common.dto.account.AccountRecordRequest;
import com.payment.gateway.common.dto.account.AccountRecordResult;
import com.payment.gateway.common.exception.BusinessException;
import com.payment.gateway.common.result.Response;
import com.payment.gateway.common.result.ResultBuilder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/account")
public class AccountController {

    @Autowired
    private AccountBusiness accountBusiness;

    @PostMapping("/operate/execute")
    public AccountRealTimeResult queryAccount(@RequestBody AccountOperateRequest accountOperateRequest) {
        AccountRealTimeResult accountRealTimeResult = null;
        try {
            accountRealTimeResult = accountBusiness.operateAccount(accountOperateRequest);
        } catch (BusinessException e) {
            return ResultBuilder.withFail(new AccountRealTimeResult(), e.getBusinessExceptionCode());
        }
        return ResultBuilder.withSuccess(accountRealTimeResult);
    }

    @PostMapping("/record/execute")
    public AccountRecordResult accountRecord(@RequestBody AccountRecordRequest accountRecordRequest) {
        AccountRecordResult result = null;
        try {
            result = accountBusiness.executeRecord(accountRecordRequest);
        } catch (BusinessException e) {
            return ResultBuilder.withFail(new AccountRecordResult(), e.getBusinessExceptionCode());
        }
        return ResultBuilder.withSuccess(result);
    }
}
