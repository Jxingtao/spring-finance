package com.payment.gateway.account.listener;

import com.payment.gateway.account.service.AccountEntryService;
import com.payment.gateway.account.service.AccountTransactionService;
import com.payment.gateway.common.base.AccountStatus;
import com.payment.gateway.common.constants.AccountBusinessCode;
import com.payment.gateway.common.constants.MessageConstant;
import com.payment.gateway.common.exception.BusinessException;
import com.payment.gateway.storage.po.AccountEntry;
import com.payment.gateway.storage.po.AccountTransaction;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.amqp.rabbit.annotation.Exchange;
import org.springframework.amqp.rabbit.annotation.Queue;
import org.springframework.amqp.rabbit.annotation.QueueBinding;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.util.CollectionUtils;
import org.springframework.util.ObjectUtils;

import java.util.List;

@Component
public class TransMessageListener {
    private final Logger logger = LoggerFactory.getLogger(this.getClass());

    @Autowired
    private AccountEntryService accountEntryService;

    @Autowired
    private AccountTransactionService accountTransactionService;

    @Autowired
    private RabbitTemplate rabbitTemplate;

    @RabbitListener(bindings = @QueueBinding(
            value = @Queue,
            exchange = @Exchange(MessageConstant.MESSAGE_EXCHANGE),
            key = MessageConstant.MESSAGE_ROUTING_KEY
    ))
    public void processActTransaction(AccountTransaction accountTransaction) {
        logger.info("监听到消息队列中入账事务实体为：{}", accountTransaction.toString());
        if (ObjectUtils.isEmpty(accountTransaction.getContextTransId())) {
            this.executeActTransaction(accountTransaction);

        } else {
            AccountTransaction contextTrans = null;
            try {
                contextTrans = accountTransactionService.getById(accountTransaction.getContextTransId());
            } catch (Exception e) {
                logger.error("查询关联事务实体信息异常", e);
                this.processRecordException(accountTransaction, new BusinessException(AccountBusinessCode.ACCOUNT_DATABASE_EXCEPTION));
                return;
            }
            if (ObjectUtils.isEmpty(contextTrans)) {
                this.processRecordException(accountTransaction, new BusinessException(AccountBusinessCode.ACT_CONTEXT_TRANS_NULL_EXCEPTION));
                return;
            }
            logger.info("查询关联事务实体信息为：{}", contextTrans.toString());

            if (AccountStatus.RECORD_SUCCESS.equals(contextTrans.getStatus())) {
                this.executeActTransaction(accountTransaction);

            } else if (AccountStatus.INITIALIZE.equals(contextTrans.getStatus())) {
                logger.info("校验关联事务未入账完成，入账事务重新推送消息队列待处理：{}", accountTransaction.toString());
                try {
                    rabbitTemplate.convertAndSend(MessageConstant.MESSAGE_EXCHANGE, MessageConstant.MESSAGE_ROUTING_KEY, accountTransaction);
                } catch (Exception e) {
                    logger.error("入账事务重新推送消息队列异常", e);
                    this.processRecordException(accountTransaction, new BusinessException(AccountBusinessCode.TRANS_MESSAGE_QUEUE_EXCEPTION));
                }

            } else {
                this.processRecordException(accountTransaction, new BusinessException(AccountBusinessCode.CONTEXT_TRANS_FAIL_EXCEPTION));
            }
        }
    }

    private void executeActTransaction(AccountTransaction accountTransaction) {
        List<AccountEntry> accountEntries = null;
        try {
            accountEntries = accountEntryService.getAccountEntries(accountTransaction.getActTransId());
        } catch (Exception e) {
            logger.error("查询入账分录列表发生异常", e);
            this.processRecordException(accountTransaction, new BusinessException(AccountBusinessCode.ACCOUNT_DATABASE_EXCEPTION));
            return;
        }
        if (CollectionUtils.isEmpty(accountEntries)) {
            this.processRecordException(accountTransaction, new BusinessException(AccountBusinessCode.ACT_ENTRY_NULL_EXCEPTION));
            return;
        }
        logger.info("查询入账分录列表内容为：{}", accountEntries.toString());

        try {
            accountEntryService.processAccountRecord(accountEntries, accountTransaction);
        } catch (Exception e) {
            logger.error("处理入账分录列表发生异常", e);
            this.processRecordException(accountTransaction, new BusinessException(AccountBusinessCode.RECORD_TRANS_FAIL_EXCEPTION));
            return;
        }
        logger.info("处理入账事务信息完成，事务实体：{}", accountTransaction.toString());
    }

    private void processRecordException(AccountTransaction accountTransaction, BusinessException e) {
        logger.error("执行入账事务发生异常，事务实体为：{}", accountTransaction.toString());

        accountTransaction.setActTransMsg(e.getBusinessExceptionCode().getMessage());
        accountTransaction.setStatus(AccountStatus.RECORD_FAIL);
        try {
            accountTransactionService.updateActTransaction(accountTransaction);
        } catch (Exception e1) {
            logger.error("执行入账异常事务数据变更失败", e1);
            return;
        }
        logger.info("执行入账异常事务数据变更完成，事务信息：{}", accountTransaction.toString());
    }
}
