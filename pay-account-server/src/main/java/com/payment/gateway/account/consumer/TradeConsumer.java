package com.payment.gateway.account.consumer;

import com.payment.gateway.account.consumer.fallback.TradeConsumerImpl;
import org.springframework.cloud.openfeign.FeignClient;

@FeignClient(name = "http://trade", path = "/trade", fallback = TradeConsumerImpl.class)
public interface TradeConsumer {
}
