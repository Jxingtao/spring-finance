package com.payment.gateway.account.consumer.fallback;

import com.payment.gateway.account.consumer.MemberConsumer;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

@Component
public class MemberConsumerImpl implements MemberConsumer {
    private final Logger logger = LoggerFactory.getLogger(this.getClass());
}
