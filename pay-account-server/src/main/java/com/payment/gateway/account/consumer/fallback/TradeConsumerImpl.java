package com.payment.gateway.account.consumer.fallback;

import com.payment.gateway.account.consumer.TradeConsumer;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

@Component
public class TradeConsumerImpl implements TradeConsumer {
    private final Logger logger = LoggerFactory.getLogger(this.getClass());
}
