package com.payment.gateway.account.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.payment.gateway.storage.po.AccountEntry;
import com.payment.gateway.storage.po.AccountTransaction;

import java.util.List;

public interface AccountEntryService extends IService<AccountEntry> {

    List<AccountEntry> getAccountEntries(Long actTransId);

    void processAccountRecord(List<AccountEntry> accountEntries, AccountTransaction accountTransaction);
}
