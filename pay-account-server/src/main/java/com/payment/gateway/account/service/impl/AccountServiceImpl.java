package com.payment.gateway.account.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.payment.gateway.account.service.AccountService;
import com.payment.gateway.common.base.*;
import com.payment.gateway.common.constants.AccountBusinessCode;
import com.payment.gateway.common.utils.GeneralUtils;
import com.payment.gateway.storage.mapper.AccountMapper;
import com.payment.gateway.storage.po.Account;
import org.springframework.stereotype.Service;

import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class AccountServiceImpl extends ServiceImpl<AccountMapper, Account> implements AccountService {

    @Override
    public List<Account> openingAccount(String memberNo) {
        List<Account> accounts = Arrays.stream(AccountType.values())
                .map(accountType -> {
                    Account account = new Account();
                    account.setActId(GeneralUtils.generateUniqueId());
                    account.setMemberNo(memberNo);
                    account.setAccountType(accountType);
                    account.setActAmount(0L);
                    account.setFreezeAmount(0L);
                    account.setWithdrawAmount(0L);
                    account.setStatus(AccountStatus.ACT_NORMAL);
                    account.setCreateTime(new Date());
                    account.setUpdateTime(new Date());
                    return account;
                }).collect(Collectors.toList());
        boolean flag = super.saveBatch(accounts);
        if (!flag) {
            throw new RuntimeException(AccountBusinessCode.ACCOUNT_DATABASE_EXCEPTION.getMessage());
        }
        return accounts;
    }

    @Override
    public void updateAccount(Account account) {
        account.setUpdateTime(new Date());
        boolean flag = super.updateById(account);
        if (!flag) {
            throw new RuntimeException(AccountBusinessCode.ACCOUNT_DATABASE_EXCEPTION.getMessage());
        }
    }

}
