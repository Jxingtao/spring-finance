package com.payment.gateway.account.business;

import com.payment.gateway.common.dto.account.AccountOperateRequest;
import com.payment.gateway.common.dto.account.AccountRealTimeResult;
import com.payment.gateway.common.dto.account.AccountRecordRequest;
import com.payment.gateway.common.dto.account.AccountRecordResult;

public interface AccountBusiness {

    /**
     * 操作账户信息
     * @param accountOperateRequest
     * @return
     */
    AccountRealTimeResult operateAccount(AccountOperateRequest accountOperateRequest);

    /**
     * 执行入账操作
     * @param accountRecordRequest
     * @return
     */
    AccountRecordResult executeRecord(AccountRecordRequest accountRecordRequest);
}
