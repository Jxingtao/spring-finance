package com.payment.gateway.trade.api;

import com.payment.gateway.common.dto.member.MemberOrderRequest;
import com.payment.gateway.common.dto.trade.TradeOrderResult;
import com.payment.gateway.common.exception.BusinessException;
import com.payment.gateway.common.result.Response;
import com.payment.gateway.common.result.ResultBuilder;
import com.payment.gateway.trade.business.TradeBusiness;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/trade")
public class TradeController {

    @Autowired
    private TradeBusiness tradeBusiness;

    @PostMapping("/order/process")
    public Response<TradeOrderResult> tradeOrder(@RequestBody MemberOrderRequest memberOrderRequest) {
        TradeOrderResult tradeOrderResult = null;
        try {
            tradeOrderResult = tradeBusiness.processOrder(memberOrderRequest);
        } catch (BusinessException e) {
            return ResultBuilder.buildFail(e.getBusinessExceptionCode());
        }
        return ResultBuilder.buildSuccess(tradeOrderResult);
    }
}
