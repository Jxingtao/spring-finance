package com.payment.gateway.trade.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.payment.gateway.common.base.TradeStatus;
import com.payment.gateway.common.constants.TradeBusinessCode;
import com.payment.gateway.common.utils.GeneralUtils;
import com.payment.gateway.storage.mapper.ReceiptMapper;
import com.payment.gateway.storage.po.Receipt;
import com.payment.gateway.trade.service.ReceiptService;
import org.springframework.stereotype.Service;

import java.util.Date;

@Service
public class ReceiptServiceImpl extends ServiceImpl<ReceiptMapper, Receipt> implements ReceiptService {

    @Override
    public Receipt getTradeOrder(String memberNo, String tradeOrderNo) {
        return super.getOne(new QueryWrapper<Receipt>()
                .lambda()
                .eq(Receipt::getMemberNo, memberNo)
                .eq(Receipt::getTradeOrderNo, tradeOrderNo));
    }

    @Override
    public Receipt initReceipt(Receipt receipt) {
        receipt.setReceiptId(GeneralUtils.generateUniqueId());
        receipt.setStatus(TradeStatus.ORDER_ACCEPT);
        receipt.setCreateTime(new Date());
        receipt.setUpdateTime(new Date());
        boolean result = super.save(receipt);
        if (!result) {
            throw new RuntimeException(TradeBusinessCode.INIT_TRADE_ORDER_EXCEPTION.getMessage());
        }
        return receipt;
    }

}
