package com.payment.gateway.trade.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.payment.gateway.storage.po.Trade;
import com.payment.gateway.storage.po.Payment;

public interface PaymentService extends IService<Payment> {

    /**
     * 执行交易支付
     * @param trade
     * @return
     */
    Payment executeTradePay(Trade trade);
}
