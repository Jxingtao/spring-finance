package com.payment.gateway.trade.business;

import com.payment.gateway.common.dto.member.MemberOrderRequest;
import com.payment.gateway.common.dto.trade.TradeOrderResult;

public interface TradeBusiness {

    TradeOrderResult processOrder(MemberOrderRequest memberOrderRequest);
}
