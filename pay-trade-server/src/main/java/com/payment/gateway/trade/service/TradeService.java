package com.payment.gateway.trade.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.payment.gateway.common.dto.account.AccountRealTimeResult;
import com.payment.gateway.storage.po.Trade;
import com.payment.gateway.storage.po.Receipt;

import java.util.concurrent.ExecutionException;
import java.util.concurrent.Future;

public interface TradeService extends IService<Trade> {

    Trade structureTrade(Receipt receipt, Future<AccountRealTimeResult> resultFuture) throws ExecutionException, InterruptedException;

}
