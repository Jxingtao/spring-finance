package com.payment.gateway.trade.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.payment.gateway.common.base.*;
import com.payment.gateway.common.constants.TradeBusinessCode;
import com.payment.gateway.common.dto.account.AccountRealTimeResult;
import com.payment.gateway.common.exception.BusinessException;
import com.payment.gateway.common.exception.BusinessExceptionCode;
import com.payment.gateway.common.exception.TradeException;
import com.payment.gateway.common.utils.GeneralUtils;
import com.payment.gateway.storage.mapper.TradeMapper;
import com.payment.gateway.storage.po.Trade;
import com.payment.gateway.storage.po.Receipt;
import com.payment.gateway.trade.service.TradeProcessService;
import com.payment.gateway.trade.service.TradeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import java.util.Date;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Future;

@Service
public class TradeServiceImpl extends ServiceImpl<TradeMapper, Trade> implements TradeService {

    @Autowired
    private TradeProcessService tradeProcessService;

    @Override
    @Transactional(propagation = Propagation.REQUIRED)
    public Trade structureTrade(Receipt receipt, Future<AccountRealTimeResult> resultFuture) throws ExecutionException, InterruptedException {
        Trade trade = GeneralUtils.convertTarget(receipt, Trade::new);
        if (BusinessType.CREDIT_COLLECTION.equals(receipt.getBusinessType())) {
            AccountRealTimeResult response = resultFuture.get();
            if (response.getSuccess()) {
                if (receipt.getTradeAmount().compareTo(response.getActAmount()) > 0) {
                    trade.setSubjectType(SubjectType.BANK_CARD_COLLECTION);
                } else {
                    trade.setSubjectType(SubjectType.BALANCE_COLLECTION);
                }
            } else {
                throw new TradeException(BusinessExceptionCode.construct(response.getResCode(), response.getMessage()));
            }
        }

        trade.setTradeId(GeneralUtils.generateUniqueId());
        trade.setMainTransId(GeneralUtils.generateUniqueId());
        trade.setMainTransStatus(TradeStatus.WAIT_RECORDING);
        trade.setRelateTransId(GeneralUtils.generateUniqueId());
        trade.setRelateTransStatus(TradeStatus.WAIT_RECORDING);
        trade.setStatus(TradeStatus.INITIALIZE);
        trade.setCreateTime(new Date());
        trade.setUpdateTime(new Date());
        boolean flag = super.save(trade);
        if (!flag) {
            throw new RuntimeException(TradeBusinessCode.INIT_TRADE_ORDER_EXCEPTION.getMessage());
        }

        switch (receipt.getSubjectType()) {
            case PAYMENT_BANK_CARD:
            case PAYMENT_BALANCE:
                tradeProcessService.executeMainTransRecord(trade);
                break;
            case BALANCE_COLLECTION:
                resultFuture = tradeProcessService.executeAccountOperate(trade.getMemberNo(), AccountType.CUSTOMER_BASE_ACT, AccountOperate.ACT_FREEZE);

                trade.setMainTransStatus(TradeStatus.FROZEN_FINISH);
                this.executeUpdate(trade);

                AccountRealTimeResult result = resultFuture.get();
                if (!result.getSuccess()) {
                    throw new BusinessException(TradeBusinessCode.EXECUTE_TRADE_PROCESS_EXCEPTION);
                }
                tradeProcessService.executeMainTransRecord(trade);
                break;
        }
        return trade;
    }

    private void executeUpdate(Trade trade) {
        trade.setUpdateTime(new Date());
        boolean flag = super.updateById(trade);
        if (!flag) {
            throw new RuntimeException(TradeBusinessCode.TRADE_DATABASE_EXCEPTION.getMessage());
        }
    }
}
