package com.payment.gateway.trade.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.payment.gateway.storage.po.Receipt;

public interface ReceiptService extends IService<Receipt> {

    Receipt getTradeOrder(String memberNo, String tradeOrderNo);

    Receipt initReceipt(Receipt receipt);

}
