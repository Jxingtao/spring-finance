package com.payment.gateway.trade.consumer.fallback;

import com.payment.gateway.common.constants.TradeBusinessCode;
import com.payment.gateway.common.dto.account.AccountOperateRequest;
import com.payment.gateway.common.dto.account.AccountRealTimeResult;
import com.payment.gateway.common.dto.account.AccountRecordRequest;
import com.payment.gateway.common.dto.account.AccountRecordResult;
import com.payment.gateway.common.exception.BusinessExceptionCode;
import com.payment.gateway.common.result.ResultBuilder;
import com.payment.gateway.trade.consumer.AccountConsumer;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

@Component
public class AccountConsumerImpl implements AccountConsumer {
    private final Logger logger = LoggerFactory.getLogger(this.getClass());

    @Override
    public AccountRealTimeResult queryAccount(AccountOperateRequest accountOperateRequest) {
        logger.error("调用账户系统账户操作接口失败{}", accountOperateRequest.toString());
        return ResultBuilder.withFail(new AccountRealTimeResult(), BusinessExceptionCode.construct(
                TradeBusinessCode.SERVICE_CALL_EXCEPTION.getResCode(), TradeBusinessCode.SERVICE_CALL_EXCEPTION.getMessage()));
    }

    @Override
    public AccountRecordResult accountRecord(AccountRecordRequest accountRecordRequest) {
        logger.error("调用账户系统账户入账接口失败{}", accountRecordRequest.toString());
        return ResultBuilder.withFail(new AccountRecordResult(), BusinessExceptionCode.construct(
                TradeBusinessCode.SERVICE_CALL_EXCEPTION.getResCode(), TradeBusinessCode.SERVICE_CALL_EXCEPTION.getMessage()));
    }
}
