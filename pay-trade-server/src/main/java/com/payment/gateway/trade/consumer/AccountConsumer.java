package com.payment.gateway.trade.consumer;

import com.payment.gateway.common.dto.account.AccountOperateRequest;
import com.payment.gateway.common.dto.account.AccountRealTimeResult;
import com.payment.gateway.common.dto.account.AccountRecordRequest;
import com.payment.gateway.common.dto.account.AccountRecordResult;
import com.payment.gateway.trade.consumer.fallback.AccountConsumerImpl;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.PostMapping;

@FeignClient(name = "http://pay-account-server", path = "/account", fallback = AccountConsumerImpl.class)
public interface AccountConsumer {

    @PostMapping("/operate/execute")
    AccountRealTimeResult queryAccount(AccountOperateRequest accountOperateRequest);

    @PostMapping("/record/execute")
    AccountRecordResult accountRecord(AccountRecordRequest accountRecordRequest);
}
