package com.payment.gateway.trade.service;

import com.payment.gateway.common.base.AccountOperate;
import com.payment.gateway.common.base.AccountType;
import com.payment.gateway.common.constants.TradeBusinessCode;
import com.payment.gateway.common.dto.account.AccountOperateRequest;
import com.payment.gateway.common.dto.account.AccountRealTimeResult;
import com.payment.gateway.storage.mapper.TradeMapper;
import com.payment.gateway.common.base.TradeStatus;
import com.payment.gateway.common.dto.account.AccountRecordRequest;
import com.payment.gateway.common.utils.GeneralUtils;
import com.payment.gateway.storage.po.Trade;
import com.payment.gateway.trade.consumer.AccountConsumer;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.Date;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.Future;

@Component
public class TradeProcessService {
    private final Logger logger = LoggerFactory.getLogger(this.getClass());

    @Autowired
    private AccountConsumer accountConsumer;

    @Autowired
    private TradeMapper tradeMapper;

    public Future<AccountRealTimeResult> executeAccountOperate(String memberNo, AccountType accountType, AccountOperate accountOperate) {
        return CompletableFuture.supplyAsync(() -> {
            AccountOperateRequest accountOperateRequest = new AccountOperateRequest();
            accountOperateRequest.setMemberNo(memberNo);
            accountOperateRequest.setAccountType(accountType);
            accountOperateRequest.setAccountOperate(accountOperate);
            return accountConsumer.queryAccount(accountOperateRequest);
        });
    }

    public void executeMainTransRecord(Trade trade) {
        AccountRecordRequest accountRecordRequest = GeneralUtils.convertTarget(trade, AccountRecordRequest::new);
        accountRecordRequest.setActTransId(trade.getMainTransId());
        CompletableFuture.supplyAsync(() -> accountConsumer.accountRecord(accountRecordRequest))
                .thenAccept(accountRecordResult -> {
                    if (accountRecordResult.getSuccess()) {
                        trade.setMainTransStatus(TradeStatus.RECORD_FINISH);
                    } else {
                        trade.setMainTransStatus(TradeStatus.RECORD_FAIL);
                    }
                    this.executeUpdate(trade);
                });

    }

    public void executeContextTransRecord(Trade trade) {
        AccountRecordRequest accountRecordRequest = GeneralUtils.convertTarget(trade, AccountRecordRequest::new);
        accountRecordRequest.setActTransId(trade.getRelateTransId());
        accountRecordRequest.setContextTransId(trade.getMainTransId());
        CompletableFuture.supplyAsync(() -> accountConsumer.accountRecord(accountRecordRequest))
                .thenAccept(accountRecordResult -> {
                    if (accountRecordResult.getSuccess()) {
                        trade.setRelateTransStatus(TradeStatus.RECORD_FINISH);
                    } else {
                        trade.setRelateTransStatus(TradeStatus.RECORD_FAIL);
                    }
                    this.executeUpdate(trade);
                });

    }

    private void executeUpdate(Trade trade) {
        trade.setUpdateTime(new Date());
        int count = tradeMapper.updateById(trade);
        if (count < 1) {
            throw new RuntimeException(TradeBusinessCode.TRADE_DATABASE_EXCEPTION.getMessage());
        }
    }
}
