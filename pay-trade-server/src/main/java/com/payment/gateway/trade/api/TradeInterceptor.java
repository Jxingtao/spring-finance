package com.payment.gateway.trade.api;

import com.payment.gateway.common.utils.GeneralUtils;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Pointcut;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

@Aspect
@Component
public class TradeInterceptor {
    private final Logger logger = LoggerFactory.getLogger(this.getClass());

    @Pointcut("execution(* com.payment.gateway.trade.api..*(..))")
    public void controllerMethodPointcut() {}

    @Around("controllerMethodPointcut()")
    public Object interceptor(ProceedingJoinPoint joinPoint) throws Throwable {
        String methodName = joinPoint.getSignature().getDeclaringTypeName().concat(".").concat(joinPoint.getSignature().getName());

        logger.info("交易系统请求方法为：{}；请求参数为：{}", methodName, GeneralUtils.writeJson(joinPoint.getArgs()));
        return joinPoint.proceed();
    }
}
