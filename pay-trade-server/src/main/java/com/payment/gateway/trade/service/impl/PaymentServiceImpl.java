package com.payment.gateway.trade.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.payment.gateway.common.base.TradeStatus;
import com.payment.gateway.common.constants.TradeBusinessCode;
import com.payment.gateway.common.utils.GeneralUtils;
import com.payment.gateway.storage.mapper.ReceiptMapper;
import com.payment.gateway.storage.mapper.TradeMapper;
import com.payment.gateway.storage.mapper.PaymentMapper;
import com.payment.gateway.storage.po.Receipt;
import com.payment.gateway.storage.po.Trade;
import com.payment.gateway.storage.po.Payment;
import com.payment.gateway.trade.service.PaymentService;
import com.payment.gateway.trade.service.TradeProcessService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import java.util.Date;

@Service
public class PaymentServiceImpl extends ServiceImpl<PaymentMapper, Payment> implements PaymentService {

    @Autowired
    private ReceiptMapper receiptMapper;

    @Autowired
    private TradeMapper tradeMapper;

    @Autowired
    private TradeProcessService tradeProcessService;

    @Override
    @Transactional(propagation = Propagation.REQUIRED)
    public Payment executeTradePay(Trade trade) {
        Payment payment = GeneralUtils.convertTarget(trade, Payment::new);
        payment.setBankActNo(trade.getBankCardNo());
        payment.setPaymentId(GeneralUtils.generateUniqueId());
        payment.setPayAmount(trade.getTradeAmount());
        payment.setBankOrderNo(GeneralUtils.generateUniqueNo());
        payment.setStatus(TradeStatus.PAY_SUCCESS);
        this.saveEntity(payment);
        switch (trade.getSubjectType()) {
            case BANK_CARD_COLLECTION:
                tradeProcessService.executeMainTransRecord(trade);
                break;
            case PAYMENT_BANK_CARD:
                tradeProcessService.executeContextTransRecord(trade);
                break;
        }

        trade.setTradeOrderNo(payment.getBankOrderNo());
        trade.setStatus(TradeStatus.TRADE_FINISH);
        trade.setUpdateTime(new Date());
        int count = tradeMapper.updateById(trade);
        if (count < 1) {
            throw new RuntimeException(TradeBusinessCode.TRADE_DATABASE_EXCEPTION.getMessage());
        }

        Receipt receipt = new Receipt();
        receipt.setReceiptId(trade.getReceiptId());
        receipt.setTradeOrderNo(trade.getTradeOrderNo());
        receipt.setSubjectType(trade.getSubjectType());
        receipt.setStatus(TradeStatus.ORDER_FINISH);
        receipt.setUpdateTime(new Date());
        count = receiptMapper.updateById(receipt);
        if (count < 1) {
            throw new RuntimeException(TradeBusinessCode.TRADE_DATABASE_EXCEPTION.getMessage());
        }

        switch (receipt.getSubjectType()) {
            case BANK_CARD_COLLECTION:
            case PAYMENT_BALANCE:
            case BALANCE_COLLECTION:
                tradeProcessService.executeContextTransRecord(trade);
                break;
        }
        return payment;
    }

    private void saveEntity(Payment payment) {
        payment.setCreateTime(new Date());
        payment.setUpdateTime(new Date());
        boolean result = super.save(payment);
        if (!result) {
            throw new RuntimeException(TradeBusinessCode.TRADE_DATABASE_EXCEPTION.getMessage());
        }
    }
}
