package com.payment.gateway.trade.business.impl;

import com.payment.gateway.common.base.AccountOperate;
import com.payment.gateway.common.base.AccountType;
import com.payment.gateway.common.base.SubjectType;
import com.payment.gateway.common.constants.MessageConstant;
import com.payment.gateway.common.constants.TradeBusinessCode;
import com.payment.gateway.common.dto.account.AccountRealTimeResult;
import com.payment.gateway.common.dto.member.MemberOrderRequest;
import com.payment.gateway.common.dto.trade.TradeOrderDTO;
import com.payment.gateway.common.dto.trade.TradeOrderResult;
import com.payment.gateway.common.exception.BusinessException;
import com.payment.gateway.common.utils.GeneralUtils;
import com.payment.gateway.storage.po.Payment;
import com.payment.gateway.storage.po.Trade;
import com.payment.gateway.storage.po.Receipt;
import com.payment.gateway.trade.business.TradeBusiness;
import com.payment.gateway.trade.service.ReceiptService;
import com.payment.gateway.trade.service.PaymentService;
import com.payment.gateway.trade.service.TradeProcessService;
import com.payment.gateway.trade.service.TradeService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.ObjectUtils;

import java.util.concurrent.Future;

@Service
public class TradeBusinessImpl implements TradeBusiness {
    private final Logger logger = LoggerFactory.getLogger(this.getClass());

    @Autowired
    private ReceiptService receiptService;

    @Autowired
    private TradeService tradeService;

    @Autowired
    private PaymentService paymentService;

    @Autowired
    private TradeProcessService tradeProcessService;

    @Autowired
    private RabbitTemplate rabbitTemplate;

    @Override
    public TradeOrderResult processOrder(MemberOrderRequest memberOrderRequest) {
        Receipt receipt = GeneralUtils.convertTarget(memberOrderRequest, Receipt::new);
        receipt.setTradeAmount(memberOrderRequest.getAmount());
        Future<AccountRealTimeResult> resultFuture = null;

        try {
            switch (memberOrderRequest.getBusinessType()) {
                case CONSUME_LOAN_PAYMENT:
                case CREDIT_LOAN_BANK_CARD:
                    receipt.setSubjectType(SubjectType.PAYMENT_BANK_CARD);
                    break;
                case CREDIT_LOAN_BALANCE:
                    receipt.setSubjectType(SubjectType.PAYMENT_BALANCE);
                    break;
                case CREDIT_COLLECTION:
                    resultFuture = tradeProcessService.executeAccountOperate(receipt.getMemberNo(), AccountType.CUSTOMER_BASE_ACT,
                            AccountOperate.ACT_QUERY);

                    Receipt stageReceipt = receiptService.getTradeOrder(memberOrderRequest.getMemberNo(), memberOrderRequest.getMainOrderNo());
                    if (ObjectUtils.isEmpty(stageReceipt)) {
                        throw new RuntimeException();
                    }
                    receipt.setSuperReceiptId(stageReceipt.getReceiptId());
                    break;
            }
            receipt = receiptService.initReceipt(receipt);
        } catch (Exception e) {
            logger.error("初始化交易订单和交易实体异常", e);
            throw new BusinessException(TradeBusinessCode.INIT_TRADE_ORDER_EXCEPTION);
        }

        Trade trade = null;
        try {
            trade = tradeService.structureTrade(receipt, resultFuture);
        } catch (Exception e) {
            logger.error("初始化交易订单和交易实体异常", e);
            throw new BusinessException(TradeBusinessCode.INIT_TRADE_ORDER_EXCEPTION);
        }

        Payment payment = null;
        try {
            payment = paymentService.executeTradePay(trade);
        } catch (Exception e) {
            logger.error("按照业务类型执行交易流程异常", e);
            throw new BusinessException(TradeBusinessCode.EXECUTE_TRADE_PROCESS_EXCEPTION);
        }

        TradeOrderResult tradeOrderResult = GeneralUtils.convertTarget(receipt, TradeOrderResult::new);
        tradeOrderResult.setTradeOrderNo(payment.getBankOrderNo());
        TradeOrderDTO tradeOrderDTO = GeneralUtils.convertTarget(tradeOrderResult, TradeOrderDTO::new);
        try {
            rabbitTemplate.convertAndSend(MessageConstant.MESSAGE_EXCHANGE, MessageConstant.MESSAGE_MEMBER_KEY, tradeOrderDTO);
        } catch (Exception e) {
            logger.error("交易订单发送到消息队列异常", e);
            throw new BusinessException(TradeBusinessCode.TRADE_MESSAGE_QUEUE_EXCEPTION);
        }
        logger.info("交易订单发送到消息队列：{}.{}完成", MessageConstant.MESSAGE_EXCHANGE, MessageConstant.MESSAGE_MEMBER_KEY);

        return tradeOrderResult;
    }

}
