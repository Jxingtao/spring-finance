package com.payment.gateway.member.api;

import com.payment.gateway.common.dto.member.*;
import com.payment.gateway.common.exception.BusinessException;
import com.payment.gateway.common.result.Response;
import com.payment.gateway.common.result.ResultBuilder;
import com.payment.gateway.member.business.MemberBusiness;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/member")
public class MemberController {

    @Autowired
    private MemberBusiness memberBusiness;

    @PostMapping("/credit/register")
    public Response<MemberRegisterResult> creditRegister(@RequestBody MemberRegisterRequest memberRegisterRequest) {
        MemberRegisterResult memberRegisterResult = null;
        try {
            memberRegisterResult = memberBusiness.creditRegister(memberRegisterRequest);
        } catch (BusinessException e) {
            return ResultBuilder.buildFail(e.getBusinessExceptionCode());
        }
        return ResultBuilder.buildSuccess(memberRegisterResult);
    }

    @PostMapping("/credit/process")
    public Response<MembershipLoanResult> creditLoan(@RequestBody MembershipLoanRequest membershipLoanRequest) {
        MembershipLoanResult membershipLoanResult = null;
        try {
            membershipLoanResult = memberBusiness.creditLoan(membershipLoanRequest);
        } catch (BusinessException e) {
            return ResultBuilder.buildFail(e.getBusinessExceptionCode());
        }
        return ResultBuilder.buildSuccess(membershipLoanResult);
    }

    @PostMapping("/credit/repay")
    public Response<MembershipRepayResult> creditRepay(@RequestBody MembershipRepayRequest membershipRepayRequest) {
        MembershipRepayResult membershipRepayResult = null;
        try {
            membershipRepayResult = memberBusiness.creditRepay(membershipRepayRequest);
        } catch (BusinessException e) {
            return ResultBuilder.buildFail(e.getBusinessExceptionCode());
        }
        return ResultBuilder.buildSuccess(membershipRepayResult);
    }

}
