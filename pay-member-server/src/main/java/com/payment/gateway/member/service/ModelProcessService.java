package com.payment.gateway.member.service;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.payment.gateway.common.base.*;
import com.payment.gateway.common.constants.MemberBusinessCode;
import com.payment.gateway.common.constants.MessageConstant;
import com.payment.gateway.common.dto.account.AccountOperateRequest;
import com.payment.gateway.common.dto.account.AccountRealTimeResult;
import com.payment.gateway.common.dto.member.MemberOrderRequest;
import com.payment.gateway.common.dto.trade.TradeOrderResult;
import com.payment.gateway.common.exception.BusinessExceptionCode;
import com.payment.gateway.common.exception.TradeException;
import com.payment.gateway.common.result.Response;
import com.payment.gateway.common.utils.GeneralUtils;
import com.payment.gateway.member.consumer.AccountConsumer;
import com.payment.gateway.member.consumer.TradeConsumer;
import com.payment.gateway.storage.po.MemberCreditReport;
import com.payment.gateway.storage.po.MembershipScore;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;

import javax.annotation.PostConstruct;
import java.io.IOException;
import java.util.Arrays;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.Future;

@Component
public class ModelProcessService {
    private final Logger logger = LoggerFactory.getLogger(this.getClass());

    @Autowired
    private MemberService memberService;

    @Autowired
    private StringRedisTemplate stringRedisTemplate;

    @Autowired
    private AccountConsumer accountConsumer;

    @Autowired
    private TradeConsumer tradeConsumer;

    @PostConstruct
    public void initMemberCreditReport() {
        memberService.queryCreditReport(MemberStatus.CREDIT_RATING).forEach(this::setRedisCreditReport);
    }

    public void setRedisCreditReport(MemberCreditReport memberCreditReport) {
        String json = null;
        try {
            json = GeneralUtils.writeJson(memberCreditReport);
        } catch (JsonProcessingException e) {
            logger.error(MemberBusinessCode.MEMBER_JSON_OPERATE_EXCEPTION.getMessage(), e);
            throw new TradeException(MemberBusinessCode.MEMBER_JSON_OPERATE_EXCEPTION);
        }
        stringRedisTemplate.opsForValue().set(MessageConstant.MEMBER_CREDIT_KEY.concat(memberCreditReport.getMemberNo()), json);
    }

    public MemberCreditReport getRedisCreditReport(String memberNo) {
        String json = stringRedisTemplate.opsForValue().get(MessageConstant.MEMBER_CREDIT_KEY.concat(memberNo));
        if (StringUtils.isEmpty(json)) {
            return null;
        }
        try {
            return GeneralUtils.readJson(json, MemberCreditReport.class);
        } catch (IOException e) {
            logger.error(MemberBusinessCode.MEMBER_JSON_OPERATE_EXCEPTION.getMessage(), e);
            throw new TradeException(MemberBusinessCode.MEMBER_JSON_OPERATE_EXCEPTION);
        }
    }

    public CreditGrade assessRiskGrade(Integer memberScore, CreditReport creditReport) {
        Integer creditScore = (memberScore + creditReport.getBase() * 100) / 6;
        return Arrays.stream(CreditGrade.values())
                .filter(creditGrade -> creditScore > creditGrade.getLevel())
                .findFirst()
                .orElse(CreditGrade.ORDINARY_CARD);
    }

    public Long assessRateAmount(Long subjectAmount, Integer subjectScore, CreditGrade creditGrade) {
        return subjectAmount * (1 + subjectScore / creditGrade.getLevel());
    }

    public Integer assessRiskScore(SubjectType subjectType, Long tradeAmount) {
        long tradeScore;
        switch (subjectType) {
            case PAYMENT_BANK_CARD:
                tradeScore = tradeAmount / 1000;
                break;
            case BANK_CARD_COLLECTION:
                tradeScore = tradeAmount / 500;
                break;
            default:
                tradeScore = tradeAmount / 2000;
        }
        return Math.toIntExact(tradeScore);
    }

    public Future<AccountRealTimeResult> asyncExecuteOpening(String memberNo) {
        return CompletableFuture.supplyAsync(() -> this.executeOpeningAccount(memberNo));
    }

    private AccountRealTimeResult executeOpeningAccount(String memberNo) {
        AccountOperateRequest accountOperateRequest = new AccountOperateRequest();
        accountOperateRequest.setMemberNo(memberNo);
        accountOperateRequest.setAccountOperate(AccountOperate.ACT_OPENING);
        logger.info("调用账户系统执行账户开通请求参数为：{}", accountOperateRequest.toString());
        Response<AccountRealTimeResult> response = accountConsumer.queryAccount(accountOperateRequest);
        logger.info("调用账户系统执行账户开通响应参数为：{}", response.toString());
        if (response.getSuccess()) {
            return response.getBody();
        }

        throw new TradeException(BusinessExceptionCode.construct(response.getResCode(), response.getMessage()));
    }

    public TradeOrderResult executeLoanTrade(MemberCreditReport memberCreditReport) {
        MemberOrderRequest memberOrderRequest = new MemberOrderRequest();
        memberOrderRequest.setMemberNo(memberCreditReport.getMemberNo());
        memberOrderRequest.setBankCardNo(memberCreditReport.getBankCardNo());
        memberOrderRequest.setBusinessType(BusinessType.CREDIT_LOAN_BANK_CARD);
        memberOrderRequest.setAmount(memberCreditReport.getMemberCredit().getCreditLine());
        return this.processMemberTrade(memberOrderRequest);
    }

    public TradeOrderResult executeRepayTrade(MemberCreditReport memberCreditReport, MembershipScore membershipScore) {
        MemberOrderRequest memberOrderRequest = new MemberOrderRequest();
        memberOrderRequest.setMemberNo(memberCreditReport.getMemberNo());
        memberOrderRequest.setMainOrderNo(membershipScore.getTradeOrderNo());
        memberOrderRequest.setBankCardNo(memberCreditReport.getBankCardNo());
        memberOrderRequest.setBusinessType(BusinessType.CREDIT_COLLECTION);
        memberOrderRequest.setAmount(membershipScore.getSubjectAmount());
        return this.processMemberTrade(memberOrderRequest);
    }

    private TradeOrderResult processMemberTrade(MemberOrderRequest memberOrderRequest) {
        logger.info("调用交易系统执行订单交易请求参数：{}", memberOrderRequest.toString());
        Response<TradeOrderResult> response = tradeConsumer.tradeOrder(memberOrderRequest);
        logger.info("调用交易系统执行订单交易响应参数：{}", response.toString());
        if (response.getSuccess()) {
            return response.getBody();
        }

        throw new TradeException(BusinessExceptionCode.construct(response.getResCode(), response.getMessage()));
    }

}
