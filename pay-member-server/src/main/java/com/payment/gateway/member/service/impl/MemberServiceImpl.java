package com.payment.gateway.member.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.payment.gateway.common.base.BusinessType;
import com.payment.gateway.common.base.MemberStatus;
import com.payment.gateway.common.constants.MemberBusinessCode;
import com.payment.gateway.common.dto.account.AccountRealTimeResult;
import com.payment.gateway.common.utils.GeneralUtils;
import com.payment.gateway.member.service.MemberService;
import com.payment.gateway.storage.mapper.MemberCreditMapper;
import com.payment.gateway.storage.mapper.MemberMapper;
import com.payment.gateway.storage.mapper.MembershipScoreMapper;
import com.payment.gateway.storage.po.Member;
import com.payment.gateway.storage.po.MemberCredit;
import com.payment.gateway.storage.po.MemberCreditReport;
import com.payment.gateway.storage.po.MembershipScore;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import java.util.Date;
import java.util.List;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Future;

@Service
public class MemberServiceImpl extends ServiceImpl<MemberMapper, Member> implements MemberService {

    @Autowired
    private MemberMapper memberMapper;

    @Autowired
    private MemberCreditMapper memberCreditMapper;

    @Autowired
    private MembershipScoreMapper membershipScoreMapper;

    @Override
    @Transactional(propagation = Propagation.REQUIRED)
    public Member registerCreditReport(MemberCreditReport memberCreditReport, Future<AccountRealTimeResult> resultFuture) {
        Member member = GeneralUtils.convertTarget(memberCreditReport, Member::new);
        member.setCreditGrade(memberCreditReport.getMemberCredit().getCreditGrade());
        member.setMemberBalance(0L);
        this.executeSave(member);

        MemberCredit memberCredit = memberCreditReport.getMemberCredit();
        memberCredit.setCreditId(GeneralUtils.generateUniqueId());
        memberCredit.setMemberId(member.getMemberId());
        memberCredit.setCreditLine((long) (memberCredit.getCreditGrade().getLevel() * 100));
        memberCredit.setStatus(MemberStatus.CREDIT_RATING);
        memberCredit.setCreateTime(new Date());
        memberCredit.setUpdateTime(new Date());
        int num = memberCreditMapper.insert(memberCredit);
        if (num < 1) {
            throw new RuntimeException(MemberBusinessCode.MEMBER_DATABASE_OPERATE_FAIL.getMessage());
        }

        try {
            resultFuture.get();
        } catch (InterruptedException | ExecutionException e) {
            throw new RuntimeException(e.getCause());
        }
        return member;
    }

    @Override
    public List<MemberCreditReport> queryCreditReport(MemberStatus memberStatus) {
        return memberMapper.queryCreditReport(memberStatus);
    }

    @Override
    @Transactional(propagation = Propagation.REQUIRED)
    public Member refreshCreditScore(MemberCreditReport memberCreditReport, MembershipScore membershipScore) {
        membershipScore.setScoreId(GeneralUtils.generateUniqueId());
        membershipScore.setMemberId(memberCreditReport.getMemberId());
        membershipScore.setStatus(MemberStatus.SCORE_FINISH);
        membershipScore.setCreateTime(new Date());
        membershipScore.setUpdateTime(new Date());
        int num = membershipScoreMapper.insert(membershipScore);
        if (num < 1) {
            throw new RuntimeException(MemberBusinessCode.MEMBER_DATABASE_OPERATE_FAIL.getMessage());
        }

        if (BusinessType.CREDIT_COLLECTION.equals(membershipScore.getBusinessType())) {
            MemberCredit memberCredit = memberCreditReport.getMemberCredit();
            memberCredit.setCreditLine((long) (memberCredit.getCreditGrade().getLevel() * 100));
            memberCredit.setUpdateTime(new Date());
            num = memberCreditMapper.updateById(memberCredit);
            if (num < 1) {
                throw new RuntimeException(MemberBusinessCode.MEMBER_DATABASE_OPERATE_FAIL.getMessage());
            }
        }

        Member member = GeneralUtils.convertTarget(memberCreditReport, Member::new);
        member.setCreditGrade(memberCreditReport.getMemberCredit().getCreditGrade());
        this.executeUpdate(member);
        return member;
    }

    private void executeSave(Member member) {
        member.setMemberId(GeneralUtils.generateUniqueId());
        member.setCreateTime(new Date());
        member.setUpdateTime(new Date());
        boolean result = super.save(member);
        if (!result) {
            throw new RuntimeException(MemberBusinessCode.MEMBER_DATABASE_OPERATE_FAIL.getMessage());
        }
    }

    private void executeUpdate(Member member) {
        member.setUpdateTime(new Date());
        boolean result = super.updateById(member);
        if (!result) {
            throw new RuntimeException(MemberBusinessCode.MEMBER_DATABASE_OPERATE_FAIL.getMessage());
        }
    }
}
