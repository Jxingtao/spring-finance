package com.payment.gateway.member.consumer;

import com.payment.gateway.member.consumer.fallback.TradeConsumerImpl;
import com.payment.gateway.common.dto.member.MemberOrderRequest;
import com.payment.gateway.common.dto.trade.TradeOrderResult;
import com.payment.gateway.common.result.Response;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.PostMapping;

@FeignClient(name = "http://trade", path = "/trade", fallback = TradeConsumerImpl.class)
public interface TradeConsumer {

    @PostMapping("/order/process")
    Response<TradeOrderResult> tradeOrder(MemberOrderRequest memberOrderRequest);
}
