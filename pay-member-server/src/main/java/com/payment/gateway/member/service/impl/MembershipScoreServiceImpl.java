package com.payment.gateway.member.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.payment.gateway.member.service.MembershipScoreService;
import com.payment.gateway.storage.mapper.MembershipScoreMapper;
import com.payment.gateway.storage.po.MembershipScore;
import org.springframework.stereotype.Service;

@Service
public class MembershipScoreServiceImpl extends ServiceImpl<MembershipScoreMapper, MembershipScore> implements MembershipScoreService {

    @Override
    public MembershipScore queryScoreReport(Long memberId, String tradeOrderNo) {
        return super.getOne(new QueryWrapper<MembershipScore>()
                .lambda()
                .eq(MembershipScore::getMemberId, memberId)
                .eq(MembershipScore::getTradeOrderNo, tradeOrderNo));
    }
}
