package com.payment.gateway.member.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.payment.gateway.member.service.MemberCreditService;
import com.payment.gateway.common.base.CreditReport;
import com.payment.gateway.common.base.MemberStatus;
import com.payment.gateway.common.constants.MemberBusinessCode;
import com.payment.gateway.common.utils.GeneralUtils;
import com.payment.gateway.storage.mapper.MemberCreditMapper;
import com.payment.gateway.storage.po.MemberCredit;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import java.util.Date;

@Service
public class MemberCreditServiceImpl extends ServiceImpl<MemberCreditMapper, MemberCredit> implements MemberCreditService {

    @Override
    @Transactional(propagation = Propagation.REQUIRED)
    public MemberCredit refreshCreditReport(MemberCredit memberCredit, CreditReport creditReport) {
        memberCredit.setCreditLine((long) (memberCredit.getCreditGrade().getLevel() * 100));
        if (creditReport.equals(memberCredit.getCreditReport())) {
            this.executeUpdate(memberCredit);
        } else {
            MemberCredit filMemberCredit = new MemberCredit();
            filMemberCredit.setCreditId(memberCredit.getCreditId());
            filMemberCredit.setStatus(MemberStatus.CREDIT_FILING);
            this.executeUpdate(filMemberCredit);

            memberCredit.setCreditReport(creditReport);
            memberCredit.setStatus(MemberStatus.CREDIT_RATING);
            this.executeSave(memberCredit);
        }
        return memberCredit;
    }

    private void executeSave(MemberCredit memberCredit) {
        memberCredit.setCreditId(GeneralUtils.generateUniqueId());
        memberCredit.setCreateTime(new Date());
        memberCredit.setUpdateTime(new Date());
        boolean result = super.save(memberCredit);
        if (!result) {
            throw new RuntimeException(MemberBusinessCode.MEMBER_DATABASE_OPERATE_FAIL.getMessage());
        }
    }

    private void executeUpdate(MemberCredit memberCredit) {
        memberCredit.setUpdateTime(new Date());
        boolean result = super.updateById(memberCredit);
        if (!result) {
            throw new RuntimeException(MemberBusinessCode.MEMBER_DATABASE_OPERATE_FAIL.getMessage());
        }
    }
}
