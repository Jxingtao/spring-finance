package com.payment.gateway.member.consumer;

import com.payment.gateway.member.consumer.fallback.AccountConsumerImpl;
import com.payment.gateway.common.dto.account.AccountOperateRequest;
import com.payment.gateway.common.dto.account.AccountRealTimeResult;
import com.payment.gateway.common.result.Response;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.PostMapping;

@FeignClient(name = "http://account", path = "/account", fallback = AccountConsumerImpl.class)
public interface AccountConsumer {

    @PostMapping("/operate/execute")
    Response<AccountRealTimeResult> queryAccount(AccountOperateRequest accountOperateRequest);
}
