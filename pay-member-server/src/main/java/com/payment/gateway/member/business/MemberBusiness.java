package com.payment.gateway.member.business;

import com.payment.gateway.common.dto.member.*;

public interface MemberBusiness {

    MemberRegisterResult creditRegister(MemberRegisterRequest memberRegisterRequest);

    MembershipLoanResult creditLoan(MembershipLoanRequest membershipLoanRequest);

    MembershipRepayResult creditRepay(MembershipRepayRequest membershipRepayRequest);
}
