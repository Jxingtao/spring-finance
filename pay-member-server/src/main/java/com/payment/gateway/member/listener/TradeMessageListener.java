package com.payment.gateway.member.listener;

import com.payment.gateway.member.service.MemberService;
import com.payment.gateway.member.service.ModelProcessService;
import com.payment.gateway.common.base.BusinessType;
import com.payment.gateway.common.constants.MemberBusinessCode;
import com.payment.gateway.common.constants.MessageConstant;
import com.payment.gateway.common.dto.trade.TradeOrderDTO;
import com.payment.gateway.common.exception.BusinessException;
import com.payment.gateway.common.utils.GeneralUtils;
import com.payment.gateway.storage.po.Member;
import com.payment.gateway.storage.po.MemberCreditReport;
import com.payment.gateway.storage.po.MembershipScore;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.amqp.rabbit.annotation.Exchange;
import org.springframework.amqp.rabbit.annotation.Queue;
import org.springframework.amqp.rabbit.annotation.QueueBinding;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.util.ObjectUtils;

@Component
public class TradeMessageListener {
    private final Logger logger = LoggerFactory.getLogger(this.getClass());

    @Autowired
    private ModelProcessService modelProcessService;

    @Autowired
    private MemberService memberService;

    @RabbitListener(bindings = @QueueBinding(
            value = @Queue,
            exchange = @Exchange(MessageConstant.MESSAGE_EXCHANGE),
            key = MessageConstant.MESSAGE_MEMBER_KEY
    ))
    public void processTradeScore(TradeOrderDTO tradeOrderDTO) {
        logger.info("监听到消息队列中交易订单实体为：{}", tradeOrderDTO.toString());
        MemberCreditReport memberCreditReport = null;
        try {
            memberCreditReport = modelProcessService.getRedisCreditReport(tradeOrderDTO.getMemberNo());
        } catch (Exception e) {
            logger.error("查询会员信用等级报告异常", e);
            this.processTradeException(tradeOrderDTO, new BusinessException(MemberBusinessCode.MEMBER_REDIS_OPERATE_EXCEPTION));
            return;
        }
        if (ObjectUtils.isEmpty(memberCreditReport)) {
            logger.info("查询会员信用等级报告为空");
            this.processTradeException(tradeOrderDTO, new BusinessException(MemberBusinessCode.MEMBER_CREDIT_REPORT_NULL));
            return;
        }

        MembershipScore membershipScore = GeneralUtils.convertTarget(tradeOrderDTO, MembershipScore::new);
        membershipScore.setSubjectAmount(tradeOrderDTO.getTradeAmount());
        membershipScore.setSubjectScore(modelProcessService.assessRiskScore(tradeOrderDTO.getSubjectType(), tradeOrderDTO.getTradeAmount()));
        logger.info("构造会员交易订单信用积分实体为：{}", membershipScore.toString());

        memberCreditReport.setMemberScore(memberCreditReport.getMemberScore() + membershipScore.getSubjectScore());
        if (BusinessType.CREDIT_COLLECTION.equals(membershipScore.getBusinessType())) {
            memberCreditReport.getMemberCredit().setCreditGrade(
                    modelProcessService.assessRiskGrade(memberCreditReport.getMemberScore(), memberCreditReport.getMemberCredit().getCreditReport()));
        }
        logger.info("同步构造会员信用等级风控模型为：{}", memberCreditReport.getMemberCredit().toString());

        Member member = null;
        try {
            member = memberService.refreshCreditScore(memberCreditReport, membershipScore);
            modelProcessService.setRedisCreditReport(memberCreditReport);
        } catch (Exception e) {
            logger.error("刷新会员信用积分信息异常", e);
            this.processTradeException(tradeOrderDTO, new BusinessException(MemberBusinessCode.REFRESH_MEMBER_SCORE_EXCEPTION));
            return;
        }
        logger.info("刷新会员信用积分信息完成：{}", member.toString());

    }

    private void processTradeException(TradeOrderDTO tradeOrderDTO, BusinessException e) {}
}
