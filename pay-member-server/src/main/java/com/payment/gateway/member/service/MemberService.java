package com.payment.gateway.member.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.payment.gateway.common.base.MemberStatus;
import com.payment.gateway.common.dto.account.AccountRealTimeResult;
import com.payment.gateway.storage.po.Member;
import com.payment.gateway.storage.po.MemberCreditReport;
import com.payment.gateway.storage.po.MembershipScore;

import java.util.List;
import java.util.concurrent.Future;

public interface MemberService extends IService<Member> {

    Member registerCreditReport(MemberCreditReport memberCreditReport, Future<AccountRealTimeResult> resultFuture);

    List<MemberCreditReport> queryCreditReport(MemberStatus memberStatus);

    Member refreshCreditScore(MemberCreditReport memberCreditReport, MembershipScore membershipScore);
}
