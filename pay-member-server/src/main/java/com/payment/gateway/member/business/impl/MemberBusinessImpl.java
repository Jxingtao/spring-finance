package com.payment.gateway.member.business.impl;

import com.payment.gateway.member.service.MemberCreditService;
import com.payment.gateway.member.service.MemberService;
import com.payment.gateway.member.service.MembershipScoreService;
import com.payment.gateway.member.service.ModelProcessService;
import com.payment.gateway.storage.po.Member;
import com.payment.gateway.storage.po.MemberCredit;
import com.payment.gateway.storage.po.MemberCreditReport;
import com.payment.gateway.storage.po.MembershipScore;
import com.payment.gateway.common.base.CreditGrade;
import com.payment.gateway.common.constants.MemberBusinessCode;
import com.payment.gateway.common.constants.MessageConstant;
import com.payment.gateway.common.dto.account.AccountRealTimeResult;
import com.payment.gateway.common.dto.member.*;
import com.payment.gateway.common.dto.trade.TradeOrderResult;
import com.payment.gateway.common.exception.BusinessException;
import com.payment.gateway.common.exception.TradeException;
import com.payment.gateway.common.utils.GeneralUtils;
import com.payment.gateway.member.business.MemberBusiness;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.ObjectUtils;

import java.util.concurrent.Future;

@Service
public class MemberBusinessImpl implements MemberBusiness {
    private final Logger logger = LoggerFactory.getLogger(this.getClass());

    @Autowired
    private MemberService memberService;

    @Autowired
    private MemberCreditService memberCreditService;

    @Autowired
    private MembershipScoreService membershipScoreService;

    @Autowired
    private ModelProcessService modelProcessService;

    @Override
    public MemberRegisterResult creditRegister(MemberRegisterRequest memberRegisterRequest) {
        MemberCreditReport memberCreditReport = GeneralUtils.convertTarget(memberRegisterRequest, MemberCreditReport::new);
        memberCreditReport.setMemberScore(MessageConstant.MEMBER_REGISTER_SCORE);
        Future<AccountRealTimeResult> resultFuture = modelProcessService.asyncExecuteOpening(memberCreditReport.getMemberNo());
        logger.info("构造会员信用报告并异步发送开通账户：{}", memberCreditReport.toString());

        MemberCredit memberCredit = GeneralUtils.convertTarget(memberRegisterRequest, MemberCredit::new);
        memberCredit.setCreditGrade(modelProcessService.assessRiskGrade(memberCreditReport.getMemberScore(), memberCredit.getCreditReport()));
        memberCreditReport.setMemberCredit(memberCredit);
        logger.info("会员信用报告风控模型评级完成：{}", memberCredit.toString());

        Member member = null;
        try {
            member = memberService.registerCreditReport(memberCreditReport, resultFuture);
            modelProcessService.setRedisCreditReport(memberCreditReport);
        } catch (Exception e) {
            logger.error("执行会员信用报告注册异常", e);
            throw new BusinessException(MemberBusinessCode.MEMBER_DATABASE_OPERATE_EXCEPTION);
        }
        logger.info("初始化会员信用报告内容完成：{}", memberCreditReport.toString());

        MemberRegisterResult memberRegisterResult = GeneralUtils.convertTarget(memberCreditReport, MemberRegisterResult::new);
        memberRegisterResult.setCreditGrade(member.getCreditGrade());

        return memberRegisterResult;
    }

    @Override
    public MembershipLoanResult creditLoan(MembershipLoanRequest membershipLoanRequest) {
        MemberCreditReport memberCreditReport = null;
        try {
            memberCreditReport = modelProcessService.getRedisCreditReport(membershipLoanRequest.getMemberNo());
        } catch (Exception e) {
            logger.error("信用借款查询会员信用报告异常", e);
            throw new BusinessException(MemberBusinessCode.MEMBER_REDIS_OPERATE_EXCEPTION);
        }
        if (ObjectUtils.isEmpty(memberCreditReport)) {
            throw new BusinessException(MemberBusinessCode.MEMBER_CREDIT_REPORT_NULL);
        }
        logger.info("信用借款查询会员信用报告信息：{}", memberCreditReport.toString());

        CreditGrade creditGrade = modelProcessService.assessRiskGrade(memberCreditReport.getMemberScore(), membershipLoanRequest.getCreditReport());
        memberCreditReport.getMemberCredit().setCreditGrade(creditGrade);
        logger.info("会员信用等级风控模型评级完成：{}", memberCreditReport.getMemberCredit().toString());

        MemberCredit memberCredit = null;
        try {
            memberCredit = memberCreditService.refreshCreditReport(memberCreditReport.getMemberCredit(), membershipLoanRequest.getCreditReport());
            modelProcessService.setRedisCreditReport(memberCreditReport);
        } catch (Exception e) {
            logger.error("刷新会员信用等级报告信息异常", e);
            throw new BusinessException(MemberBusinessCode.REFRESH_MEMBER_GRADLE_EXCEPTION);
        }
        logger.info("刷新会员信用等级报告信息完成：{}", memberCredit.toString());

        MembershipLoanResult membershipLoanResult = GeneralUtils.convertTarget(membershipLoanRequest, MembershipLoanResult::new);
        TradeOrderResult tradeOrderResult = null;
        try {
            tradeOrderResult = modelProcessService.executeLoanTrade(memberCreditReport);
        } catch (TradeException e) {
            throw new BusinessException(e.getBusinessExceptionCode());
        }
        membershipLoanResult.setLoanAmount(tradeOrderResult.getTradeAmount());

        return membershipLoanResult;
    }

    @Override
    public MembershipRepayResult creditRepay(MembershipRepayRequest membershipRepayRequest) {
        MemberCreditReport memberCreditReport = null;
        try {
            memberCreditReport = modelProcessService.getRedisCreditReport(membershipRepayRequest.getMemberNo());
        } catch (Exception e) {
            logger.error("信用还款查询会员信用报告异常", e);
            throw new BusinessException(MemberBusinessCode.MEMBER_REDIS_OPERATE_EXCEPTION);
        }
        if (ObjectUtils.isEmpty(memberCreditReport)) {
            throw new BusinessException(MemberBusinessCode.MEMBER_CREDIT_REPORT_NULL);
        }
        logger.info("信用还款查询会员信用报告信息：{}", memberCreditReport.toString());

        MembershipScore membershipScore = null;
        try {
            membershipScore = membershipScoreService.queryScoreReport(memberCreditReport.getMemberId(), membershipRepayRequest.getTradeOrderNo());
        } catch (Exception e) {
            logger.error("查询会员信用积分记录异常", e);
            throw new BusinessException(MemberBusinessCode.MEMBER_DATABASE_OPERATE_EXCEPTION);
        }
        if (ObjectUtils.isEmpty(membershipScore)) {
            throw new BusinessException(MemberBusinessCode.MEMBER_SCORE_REPORT_NULL);
        }
        membershipScore.setSubjectAmount(modelProcessService.assessRateAmount(membershipScore.getSubjectAmount(),
                membershipScore.getSubjectScore(), memberCreditReport.getMemberCredit().getCreditGrade()));
        logger.info("会员信用还款风控模型额度完成：{}", membershipScore.toString());

        TradeOrderResult tradeOrderResult = null;
        try {
            tradeOrderResult = modelProcessService.executeRepayTrade(memberCreditReport, membershipScore);
        } catch (TradeException e) {
            throw new BusinessException(e.getBusinessExceptionCode());
        }
        MembershipRepayResult membershipRepayResult = GeneralUtils.convertTarget(tradeOrderResult, MembershipRepayResult::new);
        membershipRepayResult.setRepayAmount(tradeOrderResult.getTradeAmount());

        return membershipRepayResult;
    }
}
