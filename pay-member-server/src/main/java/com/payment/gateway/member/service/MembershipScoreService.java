package com.payment.gateway.member.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.payment.gateway.storage.po.MembershipScore;

public interface MembershipScoreService extends IService<MembershipScore> {

    MembershipScore queryScoreReport(Long memberId, String tradeOrderNo);
}
