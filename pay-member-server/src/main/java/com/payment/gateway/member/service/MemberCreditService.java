package com.payment.gateway.member.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.payment.gateway.common.base.CreditReport;
import com.payment.gateway.storage.po.MemberCredit;

public interface MemberCreditService extends IService<MemberCredit> {

    MemberCredit refreshCreditReport(MemberCredit memberCredit, CreditReport creditReport);
}
