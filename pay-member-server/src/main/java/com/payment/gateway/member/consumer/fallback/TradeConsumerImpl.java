package com.payment.gateway.member.consumer.fallback;

import com.payment.gateway.common.constants.MemberBusinessCode;
import com.payment.gateway.common.dto.member.MemberOrderRequest;
import com.payment.gateway.common.dto.trade.TradeOrderResult;
import com.payment.gateway.common.exception.BusinessExceptionCode;
import com.payment.gateway.common.result.Response;
import com.payment.gateway.common.result.ResultBuilder;
import com.payment.gateway.member.consumer.TradeConsumer;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

@Component
public class TradeConsumerImpl implements TradeConsumer {
    private final Logger logger = LoggerFactory.getLogger(this.getClass());

    @Override
    public Response<TradeOrderResult> tradeOrder(MemberOrderRequest memberOrderRequest) {
        logger.error("调用交易系统交易收单接口失败{}", memberOrderRequest.toString());
        return ResultBuilder.buildFail(BusinessExceptionCode.construct(
                MemberBusinessCode.SERVICE_CALL_EXCEPTION.getResCode(), MemberBusinessCode.SERVICE_CALL_EXCEPTION.getMessage()
        ));
    }
}
