package com.payment.gateway.storage.po;

import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.payment.gateway.common.base.BusinessType;
import com.payment.gateway.common.base.SubjectType;
import com.payment.gateway.common.base.TradeStatus;
import lombok.Data;

import java.io.Serializable;
import java.util.Date;

@Data
@TableName("t_receipt")
public class Receipt implements Serializable {
    private static final long serialVersionUID = 1L;

    @TableId
    private Long receiptId;

    private Long superReceiptId;

    private String tradeOrderNo;

    private String memberNo;

    private String merchantNo;

    private String bankCardNo;

    private BusinessType businessType;

    private SubjectType subjectType;

    private Long tradeAmount;

    private TradeStatus status;

    private Date createTime;

    private Date updateTime;

}
