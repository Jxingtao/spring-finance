package com.payment.gateway.storage.po;

import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.payment.gateway.common.base.SubjectType;
import com.payment.gateway.common.base.TradeStatus;
import lombok.Data;

import java.io.Serializable;
import java.util.Date;

@Data
@TableName("t_payment")
public class Payment implements Serializable {
    private static final long serialVersionUID = 1L;

    @TableId
    private Long paymentId;

    private Long tradeId;

    private String bankActNo;

    private SubjectType subjectType;

    private Long payAmount;

    private String bankOrderNo;

    private TradeStatus status;

    private Date createTime;

    private Date updateTime;

}
