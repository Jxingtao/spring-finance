package com.payment.gateway.storage.po;

import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.payment.gateway.common.base.SubjectType;
import com.payment.gateway.common.base.TradeStatus;
import lombok.Data;

import java.io.Serializable;
import java.util.Date;

@Data
@TableName("t_trade")
public class Trade implements Serializable {
    private static final long serialVersionUID = 1L;

    @TableId
    private Long tradeId;

    private Long receiptId;

    private String tradeOrderNo;

    private String memberNo;

    private String bankCardNo;

    private Long tradeAmount;

    private SubjectType subjectType;

    private Long mainTransId;

    private TradeStatus mainTransStatus;

    private Long relateTransId;

    private TradeStatus relateTransStatus;

    private TradeStatus status;

    private Date createTime;

    private Date updateTime;

}
