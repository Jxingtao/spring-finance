package com.payment.gateway.storage.po;

import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.payment.gateway.common.base.AccountStatus;
import com.payment.gateway.common.base.SubjectType;
import lombok.Data;

import java.io.Serializable;
import java.util.Date;

@Data
@TableName("a_account_transaction")
public class AccountTransaction implements Serializable {
    private static final long serialVersionUID = 1L;

    @TableId
    private Long actTransId;

    private Long tradeId;

    private Long contextTransId;

    private String memberNo;

    private SubjectType subjectType;

    private AccountStatus status;

    private String actTransMsg;

    private Date createTime;

    private Date updateTime;

}
