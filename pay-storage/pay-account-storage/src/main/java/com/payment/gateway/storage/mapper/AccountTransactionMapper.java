package com.payment.gateway.storage.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.payment.gateway.storage.po.AccountTransaction;

public interface AccountTransactionMapper extends BaseMapper<AccountTransaction> {
}
