package com.payment.gateway.storage.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.payment.gateway.storage.po.AccountEntry;

public interface AccountEntryMapper extends BaseMapper<AccountEntry> {
}
