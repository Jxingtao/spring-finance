package com.payment.gateway.storage.po;

import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.payment.gateway.common.base.AccountStatus;
import com.payment.gateway.common.base.BalanceDirection;
import lombok.Data;

import java.io.Serializable;
import java.util.Date;

@Data
@TableName("a_account_entry")
public class AccountEntry implements Serializable {
    private static final long serialVersionUID = 1L;

    @TableId
    private Long entryId;

    private Long actTransId;

    private Long actId;

    private BalanceDirection balanceDirection;

    private Long entryAmount;

    private AccountStatus status;

    private Date createTime;

    private Date updateTime;

}
