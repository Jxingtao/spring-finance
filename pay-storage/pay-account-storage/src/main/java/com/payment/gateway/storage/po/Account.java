package com.payment.gateway.storage.po;

import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.payment.gateway.common.base.AccountStatus;
import com.payment.gateway.common.base.AccountType;
import lombok.Data;

import java.io.Serializable;
import java.util.Date;

@Data
@TableName("a_account")
public class Account implements Serializable {
    private static final long serialVersionUID = 1L;

    @TableId
    private Long actId;

    private String memberNo;

    private AccountType accountType;

    private Long actAmount;

    private Long withdrawAmount;

    private Long freezeAmount;

    private AccountStatus status;

    private Date createTime;

    private Date updateTime;

}
