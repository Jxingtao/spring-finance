package com.payment.gateway.storage.po;

import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.payment.gateway.common.base.BusinessType;
import com.payment.gateway.common.base.MemberStatus;
import com.payment.gateway.common.base.SubjectType;
import lombok.Data;

import java.io.Serializable;
import java.util.Date;

@Data
@TableName("m_membership_score")
public class MembershipScore implements Serializable {
    private static final long serialVersionUID = 1L;

    @TableId
    private Long scoreId;

    private Long memberId;

    private String tradeOrderNo;

    private BusinessType businessType;

    private SubjectType subjectType;

    private Long subjectAmount;

    private Integer subjectScore;

    private MemberStatus status;

    private Date createTime;

    private Date updateTime;

}
