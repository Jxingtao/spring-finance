package com.payment.gateway.storage.po;

import lombok.Data;

import java.io.Serializable;

@Data
public class MemberCreditReport implements Serializable {
    private static final long serialVersionUID = 1L;

    private Long memberId;

    private String memberNo;

    private String mobilePhone;

    private String bankCardNo;

    private Integer memberScore;

    private Long memberBalance;

    private MemberCredit memberCredit;

}
