package com.payment.gateway.storage.po;

import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.payment.gateway.common.base.CreditGrade;
import lombok.Data;

import java.io.Serializable;
import java.util.Date;

@Data
@TableName("m_member")
public class Member implements Serializable {
    private static final long serialVersionUID = 1L;

    @TableId
    private Long memberId;

    private String memberNo;

    private String mobilePhone;

    private String bankCardNo;

    private CreditGrade creditGrade;

    private Integer memberScore;

    private Long memberBalance;

    private Date createTime;

    private Date updateTime;

}
