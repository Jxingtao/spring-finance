package com.payment.gateway.storage.po;

import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.payment.gateway.common.base.CreditGrade;
import com.payment.gateway.common.base.CreditReport;
import com.payment.gateway.common.base.MemberStatus;
import lombok.Data;

import java.io.Serializable;
import java.util.Date;

@Data
@TableName("m_member_credit")
public class MemberCredit implements Serializable {
    private static final long serialVersionUID = 1L;

    @TableId
    private Long creditId;

    private Long memberId;

    private CreditReport creditReport;

    private CreditGrade creditGrade;

    private Long creditLine;

    private MemberStatus status;

    private Date createTime;

    private Date updateTime;

}
