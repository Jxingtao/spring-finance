package com.payment.gateway.storage.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.payment.gateway.storage.po.Member;
import com.payment.gateway.storage.po.MemberCreditReport;
import com.payment.gateway.common.base.MemberStatus;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface MemberMapper extends BaseMapper<Member> {

    List<MemberCreditReport> queryCreditReport(@Param("memberStatus") MemberStatus memberStatus);
}
